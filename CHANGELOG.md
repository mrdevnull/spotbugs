# spotbugs analyzer changelog

## v2.1.0
- Bump Spotbugs to [3.1.12](https://github.com/spotbugs/spotbugs/blob/3.1.12/CHANGELOG.md#3112---2019-02-28)
- Bump Find Sec Bugs to [1.9.0](https://github.com/find-sec-bugs/find-sec-bugs/releases/tag/version-1.9.0)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Merge of the Maven, Gradle, Groovy and SBT analyzers with additional features:
  - Use the SpotBugs CLI for analysis.
  - Report correct source file paths.
  - Handle Maven multi module projects.
  - Only report vulnerabilities in source code files, ignore dependencies' libraries.
  - Add command line parameters and environment variable to set the paths of the Ant, Gradle, Maven, SBT and Java
    executables.
  - Add the `--mavenCliOpts` command line parameter and `MAVEN_CLI_OPTS` environment to pass arguments to Maven.
